const formatMinutes = minutes => {
  return minutes < 9 ? `0${minutes}` : minutes;
};

export const getDateString = dateObj =>
  `${dateObj.getMonth()}/${dateObj.getDate()}/${dateObj.getFullYear()}`;

export const getTimeString = dateObj =>
  `${dateObj.getHours()}:${formatMinutes(dateObj.getMinutes())}`;
