export const RATE_LIMIT_MESSAGE =
  "Thank you for using Alpha Vantage! Please visit https://www.alphavantage.co/premium/ if you would like to have a higher API call volume.";

export const RATE_LIMIT_ERROR = "RATE_LIMIT_ERROR";

export const STOCKS_API_URL = `https://www.alphavantage.co/query?apikey=XVA5DY6S56RKD4BZ&function=TIME_SERIES_INTRADAY&outputsize=compact`;
