import * as d3 from "d3";

export const drawChart = (selector, data) => {
  const svgWidth = 600;
  const svgHeight = 400;
  const margin = {
    top: 20,
    bottom: 30,
    left: 50,
    right: 20
  };
  const height = svgHeight - margin.top - margin.bottom;
  const width = svgWidth - margin.left - margin.right;

  const svg = d3
    .select(selector)
    .attr("width", svgWidth)
    .attr("height", svgHeight);

  const g = svg
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  const x = d3.scaleTime().rangeRound([0, width]);
  const y = d3.scaleLinear().rangeRound([height, 0]);

  const line = d3
    .line()
    .x(function(d) {
      return x(d.dateTime);
    })
    .y(function(d) {
      return y(parseFloat(d.high));
    });

  x.domain(
    d3.extent(data, function(d) {
      return d.dateTime;
    })
  );
  y.domain(
    d3.extent(data, function(d) {
      return parseFloat(d.high);
    })
  );

  g.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x))
    .select(".domain")
    .remove();

  g.append("g")
    .call(d3.axisLeft(y))
    .append("text")
    .attr("fill", "#000")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .attr("text-anchor", "end")
    .select(".domain")
    .remove();

  g.append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", "steelblue")
    .attr("stroke-linejoin", "round")
    .attr("stroke-linecap", "round")
    .attr("stroke-width", 1.5)
    .attr("d", line);
};

export const removeChart = () => {
  d3.select("svg > *").remove();
};
