import axios from "axios";
import {
  STOCKS_API_URL,
  RATE_LIMIT_MESSAGE,
  RATE_LIMIT_ERROR
} from "./constants";
import { getDateString, getTimeString } from "./dateUtils.js";

function fetchEntries(symbol, interval = "60min") {
  return axios
    .get(STOCKS_API_URL, {
      params: {
        symbol,
        interval
      }
    })
    .then(res => {
      if (res.data["Error Message"]) {
        throw new Error(res.data["Error Message"]);
      }

      if (res.data.Information === RATE_LIMIT_MESSAGE) {
        throw new Error(RATE_LIMIT_ERROR);
      }

      return (
        res &&
        res.data &&
        res.data[`Time Series (${interval})`] &&
        Object.keys(res.data[`Time Series (${interval})`]).map(function(
          dateTime
        ) {
          const singleResult = res.data[`Time Series (${interval})`][dateTime];
          const dateObj = new Date(dateTime);
          return {
            dateTime: dateObj,
            date: getDateString(dateObj),
            time: getTimeString(dateObj),
            high: parseFloat(singleResult["2. high"], 10).toFixed(2),
            low: parseFloat(singleResult["3. low"], 10).toFixed(2)
          };
        })
      );
    })
    .then(entries => {
      this.loading = false;
      this.error = null;
      this.entries = entries;
      this.rateLimitExceeded = false;
    })
    .catch(error => {
      this.loading = false;
      this.entries = [];

      if (error.message === RATE_LIMIT_ERROR) {
        this.rateLimitExceeded = true;
        this.error = null;
      } else {
        console.error(error);

        this.rateLimitExceeded = false;
        this.error = error;
      }
    });
}

export default fetchEntries;
